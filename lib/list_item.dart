import 'package:flutter/material.dart';
import 'package:phone_only/model/tourism_place.dart';
import 'package:phone_only/provider/done_tourism_provider.dart';

class ListItem extends StatelessWidget{
  final TourismPlace place;
  final bool isDone;
  final Function(bool? value) onCheckboxClick;

  const ListItem(DoneTourismProvider data, {
    required this.place,
    required this.isDone,
    required this.onCheckboxClick
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      color: isDone ? Colors.green[200] : Colors.red[200],
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget> [
          Expanded(
            flex: 1,
            child: Image.asset(place.imageAsset)
          ),
          Expanded(
            flex: 2,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text(
                    place.name,
                    style: const TextStyle(fontSize: 16.0),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Text(place.location)
                ],
              ),
            ),
          ),
          Checkbox(
            checkColor: Colors.white,
            value: isDone, 
            onChanged: onCheckboxClick
          ),
        ],
      ),
    );
  }
}