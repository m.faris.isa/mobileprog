import 'package:flutter/material.dart';
import 'package:phone_only/done_tourism_list.dart';
import 'package:phone_only/model/tourism_place.dart';
import 'package:phone_only/tourism_list.dart';
import 'detail_screen.dart';
import 'list_item.dart';

class MainScreen extends StatefulWidget {
  MainScreen({Key? key}) : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
  
}

class _MainScreenState extends State<MainScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Wisata Jakarta'),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.done_outline),
            onPressed: () {
              Navigator.push(
                context, 
                MaterialPageRoute(builder: (context){
                  return DoneTourismList();
                }),
              );
            },
          )
        ],
      ),
      body: const TourismList()
    );
  }
}

