import 'package:flutter/material.dart';
import 'package:phone_only/detail_screen.dart';
import 'package:phone_only/list_item.dart';
import 'package:phone_only/model/tourism_place.dart';
import 'package:phone_only/provider/done_tourism_provider.dart';
import 'package:provider/provider.dart';

class TourismList extends StatefulWidget{
  const TourismList({Key? key}) : super(key: key);

  @override
  _TourismListState createState() => _TourismListState();
}

class _TourismListState extends State<TourismList>{
  final List<TourismPlace> tourismPlaceList = [
    TourismPlace(
      name: 'National Monument (Indonesia)', 
      location: 'Central Jakarta', 
      imageAsset: 'assets/images/monas.jpg',
      day: 'Open Daily',
      time: '8 AM - 4 PM GMT(+7)',
      ticket: 'IDR 5K - 40K',
      description: 'The National Monument (Indonesian: Monumen Nasional, abbreviated Monas) is a 132 m (433 ft) obelisk in the centre of Merdeka Square, Central Jakarta, symbolizing the fight for Indonesia. It is the national monument of the Republic of Indonesia, built to commemorate the struggle for Indonesian independence.',
      imageNet: 'https://asset.kompas.com/crops/Mw0op7ZT4Rox5wNq42A_RH7_YQc=/0x123:1000x623/780x390/data/photo/2017/11/20/1346015447.jpeg',
      imageNet2: 'https://gotripina.com/wp-content/uploads/2021/09/209823637_132811188928372_6710610390832100029_n.jpg',
      image1: 'assets/images/monas1.jpg',
      image2: 'assets/images/monas2.jpg'
    ),
    TourismPlace(
      name: 'Jakarta History Museum', 
      location: 'West Jakarta', 
      imageAsset: 'assets/images/fatahillah.jpg',
      day: 'Tuesday - Sunday',
      time: '9 AM - 3 PM GMT(+7)',
      ticket: 'IDR 2K - 5K',
      description: "The Jakarta History Museum (Indonesian: Museum Sejarah Jakarta), also known as Fatahillah Museum or Batavia Museum, is located in the Old Town (known as Kota Tua) of Jakarta, Indonesia. The building was built in 1710 as the Stadhuis (city hall) of Batavia. Jakarta History Museum opened in 1974 and displays objects from the prehistory period of the city region, the founding of Jayakarta in 1527, and the Dutch colonization period from the 16th century until Indonesia's Independence in 1945.",
      imageNet: 'https://aset.idetrips.com/wp-content/uploads/2018/08/kota-tua-jakarta-museum-fatahilah-wikicommons-Gunawan-Kartapranata.jpg',
      imageNet2: 'https://mmc.tirto.id/image/otf/1024x535/2020/12/02/taman-fathillah-kota-tua-tirto-mico-1_ratio-16x9.jpg',
      image1: 'assets/images/fatahillah1.jpg',
      image2: 'assets/images/fatahillah2.jpg'
    ),
    TourismPlace(
      name: 'Taman Mini Indonesia Indah', 
      location: 'East Jakarta', 
      imageAsset: 'assets/images/tmii.jpeg',
      day: 'Open Daily',
      time: '6 AM - 5 PM GMT(+7)',
      ticket: 'IDR 2K - 5K',
      description: "Taman Mini 'Indonesia Indah' (literally 'Beautiful Indonesia' Mini Park—the apostrophes are in the name—abbreviated as TMII) is a culture-based recreational area located in East Jakarta, Indonesia. Since July 2021, it is operated by PT Taman Wisata Candi Borobudur, Prambanan, dan Ratu Boko, a subsidiary of the state-owned tourism holding company InJourney. It was operated by Yayasan Harapan Kita, a foundation established by Siti Hartinah, the first lady during most of the New Order and wife of Suharto, and run by Suharto's descendants since his death until 2021. It has an area of about 100 hectares (250 acres).",
      imageNet: 'https://akcdn.detik.net.id/visual/2021/04/08/wisata-tmii_169.jpeg?w=650',
      imageNet2: 'https://img.okezone.com/content/2022/11/25/470/2715014/wajah-baru-tmii-wamen-bumn-masyarakat-punya-opsi-menikmati-jakarta-ep7NL930Hb.jpg',
      image1: 'assets/images/tmii1.jpg',
      image2: 'assets/images/tmii2.jpeg'
    ),
    TourismPlace(
      name: 'Ragunan Zoo', 
      location: 'South Jakarta', 
      imageAsset: 'assets/images/ragunan.jpg',
      day: 'Tuesday - Sunday',
      time: '7 AM - 4 PM GMT(+7)',
      ticket: 'IDR 3K - 75K',
      description: "Ragunan Zoo (Indonesian: Kebun Binatang Ragunan) is a zoo located in Pasar Minggu, South Jakarta, Indonesia. The zoo has an area of 140-hectare (350-acre). The zoo has an aviary and a primate centre, and employs over 450 people. Many of the animals in the zoo are endangered and threatened from all parts of Indonesia and the rest of the world. There are a total of 2,288 animals inside the zoo. Laid out in a lush tropical habitat, rare animals such as crocodile, gorilla, orangutan, tapir, anoa, sumatran tiger, babirusa and peacocks are given ample room. The zoo is located in South Jakarta and is easily accessible through the Jakarta Outer Ring Road and TransJakarta Corridor 6 bus (grey color).",
      imageNet: 'https://cdn1-production-images-kly.akamaized.net/pgabVdjQTpVBR52S_k5eAhWF88M=/640x360/smart/filters:quality(75):strip_icc():format(jpeg)/kly-media-production/medias/3106995/original/019351800_1587370507-20200420-Perawatan-Satwa-Ragunan-6.jpg',
      imageNet2: 'https://cdn.idntimes.com/content-images/post/20200618/whatsapp-image-2020-06-18-at-50945-pm-1-1888dec89476ccb48ccb26154e5d2814_600x400.jpeg',
      image1: 'assets/images/ragunan1.jpg',
      image2: 'assets/images/ragunan2.jpeg'
    ),
    TourismPlace(
      name: 'Sea World Jakarta', 
      location: 'North Jakarta', 
      imageAsset: 'assets/images/seaworld.jpg',
      day: 'Tuesday - Sunday',
      time: '9 AM - 4.30 PM GMT(+7)',
      ticket: 'IDR 110K - 310K',
      description: "Sea World Jakarta or also known Sea World Ancol is a marine aquarium suited in North Jakarta, Jakarta, Indonesia. It consists of a main tank, a shark tank, and several other tanks, including a turtle exhibit. The Main tank of SeaWorld Ancol is one of the biggest aquarium in Southeast Asia. SeaWorld Ancol was briefly closed in September 2014 but reopened on July 17, 2015, to the public. SeaWorld Ancol is currently expanding to turn the aquarium into the world's largest sea park.",
      imageNet: 'https://www.visitcalifornia.com/sites/visitcalifornia.com/files/VC_SeaWorldSanDiego_Supplied_SW5_0771_Final_1280x640.jpg',
      imageNet2: 'https://cdn2.tstatic.net/tribunnewswiki/foto/bank/images/sea-world-ancol-2.jpg',
      image1: 'assets/images/seaworld1.jpg',
      image2: 'assets/images/seaworld2.jpg'
    )
  ];

  @override
  Widget build(BuildContext context){
    return ListView.builder(itemBuilder: (context, index){
      final TourismPlace place = tourismPlaceList[index];
      return InkWell(
        onTap: () {
          Navigator.push(context, MaterialPageRoute(builder: (context){
            return DetailScreen(place: place);
          }));
        },
        child: Consumer<DoneTourismProvider>(
          builder: (context, data, widget) {
            var isDone = data.doneTourismPlaceList.contains(place);
            return ListItem(
              data,
              place: place,
              isDone: isDone,
              onCheckboxClick: (bool? value){
                setState(() {
                  if(value!=null){
                    value ? data.doneTourismPlaceList.add(place) : data.doneTourismPlaceList.remove(place);
                  }
                });
              }
            );
          }
        ),
      );
    }, 
    itemCount: tourismPlaceList.length
    );
  }
}

