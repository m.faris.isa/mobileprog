class TourismPlace {
  String name;
  String location;
  String imageAsset;
  String day;
  String time;
  String ticket;
  String description;
  String imageNet;
  String imageNet2;
  String image1;
  String image2;

  TourismPlace({
    required this.name,
    required this.location,
    required this.imageAsset,
    required this.day,
    required this.time,
    required this.ticket,
    required this.description,
    required this.imageNet,
    required this.imageNet2,
    required this.image1,
    required this.image2
  });
}